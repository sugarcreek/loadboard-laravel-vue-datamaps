<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// HEY REMEMBER TO PREFIX WITH /api WHEN INVOKING THESE ROUTES!!!!


Route::group(['prefix' => 'load'], function(){
    Route::get('/ppo', 'LoadController@ppo');
    Route::get('/ppostate', 'LoadController@ppoState');
});

// non-related project route info have been removed