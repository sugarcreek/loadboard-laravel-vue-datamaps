<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// PPO Load Board
Route::group(['prefix' => 'load'], function () {
    Route::get('/ppo', function () {
        return view('loads.ppo');
    });
    Route::get('/ppo/{state}', function($state) {
        return view('loads.ppodetails', ['state' => $state]);
    });
});

// non-related project route info have been removed
