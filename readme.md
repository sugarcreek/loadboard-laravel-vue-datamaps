

## A Transportation Load Board using Laravel - VueJS - datamaps.js

Working in the transportation industry has proven to me that it's much more involved than one would think at first glance.
One of the projects I worked on was notifying carriers on a day to day basis about the availability of loads that need to be moved from location A to location B. The load is already sitting on a trailer and only needs a truck to haul it.

Our web based application is written in PHP using Laravel 5.4 framework and driven by Vue.js to keep the UI in sync with the stored and, ultimately, displayed data. I’m not here to vouch for Vue.js or prove that its better or worse than any other javascript framework. I leave that to the kids who think the world needs a module to determine whether a given [number is odd or even](https://www.npmjs.com/package/is-even). (‘Nuff said). 

What were the requirements?

- The solution had to show the available loads, state by state, on any given time of the day, ie real-time.
- When a particular state was picked (clicked or tapped), details of those available loads in that state needed displayed
- Carriers had to be able to inquire about a particular load they were interested in hauling.

Not exactly rocket science, but a nice exercise on back-end data querying and front-end visualization of that data. Being new to the front-end dev work, I had to look around on the web for a bit on possible solutions, pitfalls and suggestions from those that have gone there before. [Google](https://www.google.com/search?q=datamaps+vue&oq=datamaps+vue) and [StackOverflow](https://stackoverflow.com/search?q=datamaps) are a blessing in such cases. No need for a lot of words on why [datamaps.js](http://datamaps.github.io/) was chosen to implement our solution.

I modified package.json with (I could have issued a ‘yarn add datamaps.js’):

```json
"dependencies": {
    "datamaps": "^0.5.9"
},
```

Registered the datamaps component in `resources/assets/js/app.js`:

```require('datamaps'); ```

The required, additional routes were added to `routes/web.php`:

```php
Route::group(['prefix' => 'load'], function () {
	// Show the available loads on the map
    Route::get('/ppo', function () {
        return view('loads.ppo');
    });
    // show the detail page for a given state
    Route::get('/ppo/{state}', function($state) {
        return view('loads.ppodetails', ['state' => $state]);
    });
});
```

And the home page (`resources/views/home.blade.php`) was updated with the additional link to the map :

```<h4><a href="/load/ppo">Power Only Loads</a></h4> ```

This covers the rapid development part of the project. ‘Yarn’ rebuilds the project in no time.
The real work now is creating the 

- [ ] ​	Blade templates for the ppo and ppodetails views in the newly added ‘loads’ folder.

- [ ] ​	Create the controller to fetch the back-end data from the database server
- [ ] ​	Create the VueJS files to show the retrieved data

The Blade template for the page showing the map `resources/views/loads/ppo.blade.php`:

```php
@extends('layouts.app')

@section('content')
    @component('components.header')
        @slot('title')
            Power Only Loads
        @endslot
    @endcomponent

    <div class="container">
        <div class="well">
            <div class="row">
                <h3 style="text-align:center;">Available PPO Loads</h3>
                <ppo-loads></ppo-loads>
            </div>
        </div>
    </div>
@endsection
```

`layouts/app.blade.php` is our main application layout template, describing the `header`, `footer` and `content` sections. The `content` section is filled out by the above code, specifying a title and a container with a customized tag, representing the Vue component `ppo-loads`.

`resources/assets/js/app.js` is also updated with the component registration of `ppo-loads` and its details:

```javascript
Vue.component('ppo-loads', require('./components/PPOLoads.vue'));
Vue.component('ppo-load-details', require('./components/PPOLoadDetails.vue'));
```

A typical Vue file consists of 2 parts, a template section and a script section that acts upon the template.

```html
<template>
</template>
<script>
</script>
```

We want our page to show 

- ​	a title, centered at the top of the page
- ​	The US map with clearly marked state lines
- ​	a textual representation of the data on the map, sorted in 2 columns, state by state

Our template becomes

```php
<template>
  <div>
    <div>
        <table class="table" border="0">
            <tr>
                <td width="40%">&nbsp;</td>
                <td width="35%">Total number of Loads : <strong>{{total}}</strong></td>
                <td><div id="refreshtime"></div></td>
            </tr>
        </table>
    </div>
    <div>
        <div id="PPOLoadsData" class="col-xs-3 col-md-3">
            <!-- Show data for states A-M -->
            <table class="table">
                <thead>
                    <tr>
                        <th>Origin<br> State</th>
                        <th>Load<br> Count</th>
                    </tr>
                </thead>
                <tbody>
                	<tr v-for="order in getPPOLoadsAM()" align="center">
                        <td align="center">
                            <a v-bind:href="'/load/ppo/'+order.OROST">{{order.OROST}}</a>
                    	</td>
                        <td align="center">
                            <a v-bind:href="'/load/ppo/'+order.OROST">{{order.NOOFLOADS}}</a>
                    	</td>
                    </tr>
                </tbody>
            </table>
            <!-- Show data for states N-Z -->
            <table class="table">
                <thead>
                    <tr>
                        <th>Origin<br> State</th>
                        <th>Load<br> Count</th>
                    </tr>
                </thead>
                <tbody>
                <tr v-for="order in getPPOLoadsNZ()" align="center">
                        <td align="center">
                            <a v-bind:href="'/load/ppo/'+order.OROST">{{order.OROST}}</a>
                    	</td>
                        <td align="center">
                            <a v-bind:href="'/load/ppo/'+order.OROST">{{order.NOOFLOADS}}</a>
                    	</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- The container for the US map-->
        <div id="PPOLoadsMap" class="col-xs-9 col-md-9" 
             style="position:relative; right: 0px; height:500px;">
        </div>
    </div>
  </div>
</template>
```

The PPOLoadsMap container serves as a placeholder for the US datamap.
The javascript that fetches the data and throws it in the template, is as follows

```javascript
  import * as Datamap from 'datamaps';

  export default {
      data: function() {
        return {
            orders: [],
            total: 0
        };
      },
      mounted() {
          this.construct();
      },
      methods: {
        refreshTime: function() {
            var today = new Date();
            document.getElementById('refreshtime').innerHTML = '<a href="#" onclick="window.location.reload(true);"><span class="glyphicon glyphicon-refresh"></span></a> Last refresh time: <strong>' + today.toLocaleTimeString() + '</strong>';
        },
        construct() {
           axios.get('/api/load/ppo')  // through api.php
                .then( (response) => { 
                        this.orders = response.data;
                        // orders fix for MX state (Mexico -> TX)
                        var index = -1;
                        for (var i=0, len=this.orders.length; i<len; i++) {
                            if (this.orders[i].OROST == 'MX') {
                                index = i;
                                // add loads to TX ...
                                for (var j=0, len=this.orders.length; j<len; j++) {
                                    if (this.orders[j].OROST == 'TX') {
                                        this.orders[j].NOOFLOADS = parseInt(this.orders[j].NOOFLOADS) + parseInt(this.orders[i].NOOFLOADS);
                                    }
                                }
                            }
                        }
                        // ... and remove the MX entry
                        if (index > -1) {
                            this.orders.splice(index, 1);
                        }

                        //some Eastcoast states are so small it's hard to distinguish them 
					  // on the map, hence their name is still shown (not overwritten) 
                        // when no loads are available
                        var stateMap = {
                                'AL': ' ' , 'MT': ' ' , 'AK': ' ', 'NE': ' ' , 'AZ': ' ' , 'NV': ' ', 'AR': ' ' , 'NH': 'NH',
                                'CA': ' ' , 'NJ': 'NJ', 'CO': ' ', 'NM': ' ' , 'CT': 'CT', 'NY': ' ', 'DE': 'DE', 'NC': ' ',
                                'FL': ' ' , 'ND': ' ' , 'GA': ' ', 'OH': ' ' , 'HI': 'HI', 'OK': ' ', 'ID': ' ' , 'OR': ' ',
                                'IL': ' ' , 'PA': ' ' , 'IN': ' ', 'RI': 'RI', 'IA': ' ' , 'SC': ' ', 'KS': ' ' , 'SD': ' ',
                                'KY': ' ' , 'TN': ' ' , 'LA': ' ', 'TX': ' ' , 'ME': ' ' , 'UT': ' ', 'MD': 'MD', 'VT': 'VT',
                                'MA': 'MA', 'VA': ' ' , 'MI': ' ', 'WA': ' ' , 'MN': ' ' , 'WV': ' ', 'MS': ' ' , 'WI': ' ',
                                'MO': ' ' , 'WY': ' ' , 'DC': 'DC'
                        };

                        let total = 0;
                        for (var i=0, len=this.orders.length; i<len; i++) {
                            this.total += parseInt(this.orders[i].NOOFLOADS);
                            if (stateMap[this.orders[i].OROST] != ' ') {
                                // 'petite' east coast states
                                stateMap[this.orders[i].OROST] += (' - ' + this.orders[i].NOOFLOADS);  // NJ - 5
                            } else {
                                stateMap[this.orders[i].OROST] = this.orders[i].NOOFLOADS;
                            }
                        };

                        var ppo_map = new Datamap({
                            element: document.getElementById("PPOLoadsMap"),
                            scope: 'usa',
                            projection: 'equirectangular',
                            geographyConfig: {
                                highlightFillColor: '#c0c0c0',
                                highlightBorderColor: '#c41230',
                                highlightBorderWidth: 2,
                            },
                            fills: {
                                defaultFill: '#95a5a6'
                            },
                            done: function(datamap) {
                                    datamap.svg.selectAll('.datamaps-subunit')
                                               .on('click', function(geography) {
                                                   // click on a US state (referred to as 'geography')
                                                   // geography.id = AR - geography.properties.name = Arkansas
                                                   //alert(geography.id + '-' + geography.properties.name);
                                                   window.location.href = '/load/ppo/' + geography.id;
                                    });
                            }
                        });
                        ppo_map.labels({'customLabelText': stateMap});
                        this.refreshTime();
                }) // response promise
                .catch(function (error) { console.log(error.response.data); });
        },
        getPPOLoadsAM() {
           return this.orders.filter(order => {
                return order.OROST.match(/^[A-M].*/);
           });
        },
        getPPOLoadsNZ() {
           return this.orders.filter(order => {
                return order.OROST.match(/^[N-Z].*/);
           });
        }
    }
  }
```

We use [axios](https://github.com/axios/axios), a Promise based HTTP client to trigger the back-end Laravel controller to retrieve the data from the database. When the Promise succeeds, the returned response data is stored in the orders array for further tweaking.
The API routes, handling the axios calls, are defined in `routes/api.php` as follows :

```php
Route::group(['prefix' => 'load'], function(){
    Route::get('/ppo', 'LoadController@ppo')->middleware('auth:api');
    Route::get('/ppostate', 'LoadController@ppoState')->middleware('auth:api');
});
```



Orders going in and coming from Mexico stop at the border in TX, so all loads originating in ‘MX’ are added to the TX loads and the originals are removed from the list.
We chose to show amount of loads in each state instead of state name abbreviation for that state, except for the tiny East coast states to avoid confusion as to what load is associated with what state.

The end result (branding has been removed as much as possible)

![ppo-loads-hover.png](./ScreenDumps/ppo-loads-hover.png)

Of course, we still need to write our back-end controller that handles the actual database connection and queries.
Explanations are in the code.

`app/Http/Controllers/LoadController.php`

```php
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use App\Order;
use DateTime;
use DB;
use View;
use Log;
use Zipper;

class LoadController extends Controller
{
    public function ppo(Request $request)
    {
        Log::info('LoadController::PPO Loads count and counts per state.');
		
        // dates are stored in Julian format on our DB server
        $julian_today= date('Yz') + 1; 
        
        //Instate CA only, no outbound loads, branding removed as well
        $where_ca = "ORPDAT >= $julian_today AND ORADIN='PPO' 
                                             AND OROST='CA' AND ORDST='CA'
                                             AND ORSTAT='A'"; //order status = 'Available'

        $california = DB::connection('OurConnection')
            	->table('ORDER')
                ->select( DB::raw('OROST, count(*) as NOOFLOADS') )
            	// replace city abbreviations with city full name (originating state)
                ->join('CITIES as ORG', function ($join) { 
                    $join->on('OROST' , '=' , 'ORG.CIST');
                    $join->on('OROCTY', '=' , 'ORG.CICTY');
                })
                ->whereRaw($where_ca)
                ->groupBy('OROST');
	    
        // Rest of the US states, branding removed
        $where = "ORPDAT >= $julian_today AND ORADIN='PPO' AND OROST<>'CA'
                                          AND ORSTAT='A'";

        $allOthers = DB::connection('OurConnection')
                ->table('ORDER')
                ->select( DB::raw('OROST, count(*) as NOOFLOADS') )
            	// replace city abbreviations with city full name (originating state)
                ->join('CITIES as ORG', function ($join) {
                    $join->on('OROST' , '=' , 'ORG.CIST');
                    $join->on('OROCTY', '=' , 'ORG.CICTY');
                })
                ->whereRaw($where)
                ->groupBy('OROST');
        
        // Combine the results of both queries
        $orders = $california->union($allOthers)
                             ->orderBy('OROST')
                             ->get()
                             ->toArray();
        
        return $orders;
    }


    public function ppoState(Request $request)
    {
        Log::info('LoadController::PPO Loads per state : '.$request->input('state'));

        $sort = $request->input('sort');
        $sortDirection = $request->input('direction', 'ASC');

        // take MX into account when requested state is TX
        $detailsState = $request->input('state') == 'TX' 
                ? "AND (OROST = 'TX' OR OROST = 'MX')" 
                : ($request->input('state') == 'CA' 
	    	            ? "AND (OROST = 'CA' AND ORDST = 'CA')"
    	    	        : "AND OROST = '".$request->input('state')."'");
        
	    // dates are stored in Julian format on our DB server
        $julian_today= date('Yz') + 1;
        
        // loads marked 'Available'
        $where = "ORPDAT >= $julian_today AND ORADIN='PPO' 
                                          AND ORSTAT='A' ".
                                          $detailsState;
        
        $orders = DB::connection('OurConnection')
            	->table('ORDER')
                ->select('ORPDAT','ORAPDT','ORAPTM', 'OROCTY', 'ORG.CINAME as ORGCITY', 'OROST', 
                         'ORDCTY','DEST.CINAME as DESTCITY', 'ORDST', 'ORDDAT','ORSTP# as ORSTOPS',
                         'ORWGT','ORODR# as ORDERNO','ORMILE')
            	// replace city abbreviations with city full name (originating state)
                ->join('CITIES as ORG', function ($join) {
                    $join->on('OROST' , '=' , 'ORG.CIST');
                    $join->on('OROCTY', '=' , 'ORG.CICTY');
                })
            	// replace city abbreviations with city full name (destination state)
                ->join('CITIES as DEST', function ($join) {
                    $join->on('ORDST' , '=' , 'DEST.CIST');
                    $join->on('ORDCTY', '=' , 'DEST.CICTY');
                })
            	// sort on column clicked
                ->when($sort, function ($query) use ($sort, $sortDirection){
                    return $query->orderBy($sort, $sortDirection);
                })
                ->whereRaw($where)
                ->orderBy('ORPDAT', 'ASC')  // order results on date and time
                ->orderBy('ORPTIM', 'ASC')  // from now -> future
                ->get()
                ->toArray();
        return $orders;
    }
    
}

```

Now, lets click/tap a state that has a few loads available for hauling. Pick Georgia.

![ppo-loads-details.png](./ScreenDumps/ppo-loads-details.png)

The [pagination module](https://www.npmjs.com/package/vuejs-paginate) has been configured for 10 records per page. Data can be filtered with a minimal of clicks/taps and will be on the fly without additional queries.

![ppo-loads-details-filtered.png](./ScreenDumps/ppo-loads-details-filtered.png)

Clicking an order will popup information about that order/load and how to contact us to facilitate hauling it.
The code for enabling these details:

`resources/views/ppodetails.blade.php`

```php
@extends('layouts.app')

@section('content')
    @component('components.header')
        @slot('title')
	        Power Only Load Details
        @endslot
    @endcomponent

    <div class="container">
        <div class="well">
            <div class="row">
                <table border="0">
                    <tr>
                        <td style="width:45%;">&nbsp;</td>
                        <td style="width:35%;" nowrap><h4>PPO Load Details - {{ $state }}</h4></td>
                        <td style="width:auto;"><div id="refreshtime"></div></td>
                    </tr>
                </table>
                <ppo-load-details :state="{{json_encode($state)}}"></ppo-load-details>
            </div>
        </div>
    </div>
@endsection
```











​	

