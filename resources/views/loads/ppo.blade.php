@extends('layouts.app')

@section('content')

  @component('components.header')
      @slot('title')
          Power Only Loads
      @endslot
  @endcomponent

  <div class="container">
    <div class="well">
      <div class="row">
          <h3 style="text-align:center;">Available PPO Loads</h3>
          <ppo-loads></ppo-loads>
      </div>
    </div>
  </div>
@endsection
