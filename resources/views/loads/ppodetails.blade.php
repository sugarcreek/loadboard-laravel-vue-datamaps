@extends('layouts.app')

@section('content')

  @component('components.header')
      @slot('title')
          Power Only Load Details
      @endslot
  @endcomponent

  <div class="container">
    <div class="well">
      <div class="row">
        <table class="table" border="0">
            <tr>
                <td width="40%">&nbsp;</td>
                <td width="35%"><h4>PPO Load Details - {{ $state }}</h4></td>
                <td><div id="refreshtime"></div></td>
            </tr>
        </table>
        <ppo-load-details :state="{{json_encode($state)}}"></ppo-load-details>
      </div>
    </div>
  </div>
@endsection