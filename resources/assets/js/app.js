
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('datamaps');

import Vuelidate from 'vuelidate';
import Axios from 'axios';
import * as uiv from 'uiv';
import 'babel-polyfill';

Vue.use(Vuelidate);
Vue.use(Axios);
Vue.use(uiv);


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


Vue.component('ppo-loads', require('./components/PPOLoads.vue'));
Vue.component('ppo-load-details', require('./components/PPOLoadDetails.vue'));

Vue.filter('OurDBServerDate', function(value) {
        //console.log(value);
        var now = value;
        var start = new Date(now.getFullYear(), 0, 0);
        var diff = now - start;
        var oneDay = 1000 * 60 * 60 * 24;
        var day = Math.floor(diff / oneDay);
        return now.getFullYear().toString() + day
})

// non-related project info has been removed