<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use App\Order;
use DateTime;
use DB;
use View;
use Log;
use Zipper;

class LoadController extends Controller
{
    /*
        Returns available loads that aren't currently filled
        @return Array
     */
    public function available(Request $request)
    {
        $date = $request->input('pickup');
        $loads = Order::from('ITSR5FILE.ORDER as Order')
            ->join('ITSR5FILE.ORDERX as Extension', 'Order.ORODR#', '=', 'Extension.ORODR#')
            ->where('ORPDAT', '>=', $date)
            ->where('ORSTAT', 'A')
            ->where('ORPDRV', '      ')
            ->where('ORTYPX', 'Y')
            ->orderBy('ORPDAT', 'ASC')
            ->get();

        return $loads->toArray();
    }

    /*
        Provides all orders available for pickup
        @return array -> Dates, Origins, Destinations
     */
    public function pickup(Request $request)
    {
        // Dates for the range of orders
        $beginDate = $request->input('begin');
        $endDate = $request->input('end');

        $selectionDates = Order::from('ITSR5FILE.ORDER as Order')
            ->where('ORPDAT', '>=' , $beginDate)
            ->join('ITSR5FILE.ORDERX as Extension', 'Order.ORODR#', '=', 'Extension.ORODR#')
            ->where('ORPDAT', '<=' , $endDate)
            ->where('ORSTAT', 'A')
            ->where('ORPDRV', '      ')
            ->where('ORTYPX', 'Y');

        $dates = $selectionDates
                ->orderBy('ORPDAT', 'ASC')
                ->get(['ORPDAT'])
                ->unique('ORPDAT');

        $originStates = $selectionDates
                ->orderBy('OROST', 'ASC')
                ->get(['OROST'])
                ->unique('OROST');

        $destinationStates = $selectionDates
                ->orderBy('ORDST', 'ASC')
                ->get(['ORDST'])
                ->unique('ORDST');

        return [
            'dates' => $dates->toArray(),
            'origin' => $originStates->toArray(),
            'destination' => $destinationStates->toArray()
        ];
    }

    /*
    *Provides a summary of available loads. Filterable by sort, and search filters.
    *@return array() -> Loads, Count
    */
    public function traceSummary(Request $request)
    {

        $user = Auth::user();
        $account = $user->account_code;
        $cccs = $user->cccs()->get()->map(function($item, $key){
            return $item->ccc;
        })->toArray();
        $status = $request->input('status', 'all');
        $sort = $request->input('sort');
        $sortDirection = $request->input('direction', 'ASC');
        $display = $request->input('itemsDisplay', 10);
        $where = "";
        $trailer = "";
        $filters = $request->input('filters', []);
        $count = 0;
        foreach ($filters as $item){
            $converted = json_decode($item);
            // Log::info(print_r($converted, true));
            if($count != 0)
            {
                $where .= ' AND ';
            }
            else
                $count++;
            // if(isset($converted->selectedFilter->alias))
            // {
            //     $where .= $converted->selectedFilter->alias.'.';
            // }
            if ($converted->selectedFilter->field == 'TRTRLR') $trailer = '%'.$converted->selectedSearch.'%';
            else $where .=  $converted->selectedFilter->field . " LIKE '%" .  $converted->selectedSearch . "%' ";
        }

        $loads = Order::select(['ORODR#', 'ORCSH#', 'ORCNS#', 'ORSTAT',
                            'OROCTY', 'OROST', 'ORCUST', 'ORCONS',
                            'ORPDAT', 'ORPTIM',
                            'ORDCTY', 'ORDST', 'ORDDAT','ORDTIM'
                            ])
                    ->with('trailer')
                    ->where('ORODR#', 'NOT LIKE' , 'M%')
                    ->when($sort, function ($query) use ($sort, $sortDirection){
                        return $query->orderBy($sort, $sortDirection);
                    })
                    ->whereIn('ORBILL', $cccs);
                    if($status !== 'all') {
                        $loads->where('ORSTAT', $status);
                    }

        if($trailer !== '')
        {
            $loads->whereHas('trailer', function ($query) use ($trailer) {
                $query->where('TRTRLR', 'LIKE', $trailer);
            });
        }

        if($where !== '')
        {
            $loads = $loads->whereRaw($where);
        }

        $count = $loads->count();
        $loads = $loads->simplePaginate($display);

        $loads->map(function ($t) {
            $shipper = DB::connection('as400')->table('ITSR5FILE.CUSTMAST')->select('CUNAME')->where('CUCODE', $t->ORCUST)->first();
            if ($shipper) {
                $t['SHIPPER'] = $shipper->CUNAME;
            }
            else {
                $t['SHIPPER'] = '';
            }

            $consignee = DB::connection('as400')->table('ITSR5FILE.CUSTMAST')->select('CUNAME')->where('CUCODE', $t->ORCONS)->first();
            if ($consignee) {
                $t['CONSIGNEE'] = $consignee->CUNAME;
            }
            else {
                $t['CONSIGNEE'] = '';
            }

            $orgcity = DB::connection('as400')->table('ITSR5FILE.CITIES')
                                              ->select('CINAME')
                                              ->where('CIST', $t->OROST)
                                              ->where('CICTY', $t->OROCTY)
                                              ->first();
            if ($orgcity) {
                $t['ORGCITY'] = $orgcity->CINAME;
            }
            else {
                $t['ORGCITY'] = '';
            }

            $destcity = DB::connection('as400')->table('ITSR5FILE.CITIES')
                                               ->select('CINAME')
                                               ->where('CIST', $t->ORDST)
                                               ->where('CICTY', $t->ORDCTY)
                                               ->first();
            if ($destcity) {
                $t['DESTCITY'] = $destcity->CINAME;
            }
            else {
                $t['DESTCITY'] = '';
            }

            return $t;
        });

        // dd($test);

        // $loads = Order::from('ITSR5FILE.ORDER as ORDER')
        //     ->select(['ORODR#', 'ORCSH#', 'ORCNS#', 'ORSTAT',
        //      'CUST1.CUNAME as SHIPPER', 'OROCTY', 'OROST',
        //      'ORPDAT', 'ORPTIM', 'CUST2.CUNAME as CONSIGNEE',
        //      'ORDCTY', 'ORDST', 'ORDDAT','ORDTIM',
        //      'ORG.CINAME as ORGCITY', 'DEST.CINAME as DESTCITY'
        //      ])
        //     ->leftjoin('ITSR5FILE.CUSTMAST as CUST1', 'CUST1.CUCODE', '=' , 'ORDER.ORCUST')
        //     ->leftjoin('ITSR5FILE.CUSTMAST as CUST2', 'CUST2.CUCODE', '=' , 'ORDER.ORCONS')
        //     ->join('ITSR5FILE.CITIES as ORG', function ($join) {
        //         $join->on('OROST' , '=' , 'ORG.CIST');
        //         $join->on('OROCTY' , '=' , 'ORG.CICTY');
        //     })
        //     ->join('ITSR5FILE.CITIES as DEST', function ($join) {
        //         $join->on('ORDST' , '=' , 'DEST.CIST');
        //         $join->on('ORDCTY' , '=' , 'DEST.CICTY');
        //     })
        //     ->with('trailer')
        //     ->where('ORODR#', 'NOT LIKE' , 'M%')
        //     ->when($sort, function ($query) use ($sort, $sortDirection){
        //         return $query->orderBy($sort, $sortDirection);
        //     })
        //     ->whereIn('ORDER.ORBILL', $cccs);
        //     if($status !== 'all')
        //     {
        //         $loads->where('ORDER.ORSTAT', $status);
        //     }

        //     if($where !== '')
        //     {
        //         $loads = $loads->whereRaw($where);
        //     }
        //     $count = $loads->count();
        //     $loads = $loads
        //     ->simplePaginate($display)
        //     ->toArray();

        return ['loads' => $loads->toArray(), 'count' => $count ];
    }

    public function traceParts(Request $request)
    {
        // Log::info('It ran');
        $plantCode = $request->input('plant') ?: 'None';
        $plant = str_pad($plantCode, 6);
        $delivered = $request->input('showDelivered') == 'true' ? 'Y' : 'N';
        $days = $request->input('deliverySpan') ?: 1;
        $display = $request->input('numberDisplay', 10);

        $user = Auth::user();
        $account = str_pad($user->username, 10, ' ');
        $norm = $user->username;
        $sort = $request->input('sort');
        $sortDirection = $request->input('direction', 'ASC');
        $selectedPart  = $request->input('partNumber');

        $test = "CALL WEBLIB.MTDTRACEC2('".$account."Y".$delivered."".$days."0".$plant."')";
        // Log::info($test);
        $call = DB::connection('as400')->statement(DB::raw("CALL WEBLIB.MTDTRACEC2('".$account."Y".$delivered."".$days."0".$plant."')"));
        $parts = DB::connection('as400')
            ->table("WEBTEMP.".strtoupper($account))
            ->when($sort, function($query) use ($sort, $sortDirection){
                return $query->orderBy($sort, $sortDirection);
            })
            ->when($selectedPart, function ($query) use ($selectedPart){
                return $query->where('DPPART', 'LIKE', '%'.$selectedPart.'%');
            });
        $count = $parts->count();
        $parts = $parts->simplePaginate($display)->toArray();
        // ->select(
        //     DB::raw("SELECT * FROM WEBTEMP.".$account." WHERE DPPART LIKE '%".$selectedPart."%'")
        // );

        return ['parts' => $parts, 'count' => $count];
    }

    public function traceLoad(Request $request)
    {
        $load = $request->input('load');

        if(isset($load))
        {
            $delete = DB::connection('as400')->table('ITSR5FILE.EVMGL1OUT')->delete();
            // $call1 = DB::connection('as400')->statement(DB::raw("CALL PAMR5CSTM.EVMGL1WEB('$load')"));
            $call = DB::connection('as400')->statement(DB::raw("CALL PAMR5CSTM.EVMGL1WEB('$load')"));
            $loadDetail = DB::connection('as400')->table('ITSR5FILE.EVMGL1OUT')
                                                 ->where('SEVSOORD', 'LIKE', $load)
                                                 ->get();
            $b = '1';
            $delete = DB::connection('as400')->table('WEBLIB.WLOCATION')->delete();
            $callLocation = DB::connection('as400')->statement(DB::raw("CALL WEBLIB.LOADTRACE('$load', '$b')"));
            $loadLocation = DB::connection('as400')->table('WEBLIB.WLOCATION')->get();

            return [
                'loadDetail' => $loadDetail ,
                'loadLocation' => $loadLocation
            ];
        }
        else
            return 'No load number provided';
    }

    public function getPOD(Request $request)
    {
        $pods = Storage::files('public/pods');
        Storage::delete($pods);
        $zipPods = Storage::files('public/zip');
        Storage::delete($zipPods);

        $order = $request->input('order');
        $orderInputs = DB::connection('as400')
                        ->table('RVILIB.RVABREN9')
                        ->select('ABAICD as file_path', 'ABAJCD as file_name')
                        ->where('ABABCD', $order)
                        ->where('ABAHCD', '1')
                        ->first();

        // dd($orderInputs);

        if ($orderInputs === null) {
            return response('<center><h2>There is no POD Document scanned into the system at this time, please email pod@pamt.com for a copy</h2></center>', 200)
                            ->header('Content-Type', 'text/html');
        }

        $filePath = storage_path('app/public/pod') . trim(substr($orderInputs->file_path, 8));
        $fileName = trim(substr($orderInputs->file_name, 0, -4));

        // $tif = $fileName . '.tif';
        $tif = trim($orderInputs->file_name) . '.tif';
        $searchFile = $filePath . '/' . $fileName . '.*';

        $files = glob($searchFile);

        foreach ($files as $file) {
            Storage::putFileAs('public/pods', new File($file), $tif);
        }

        $tifFiles = glob(storage_path('app/public/pods/*'));

        Zipper::make('storage/zip/pod.zip')->add($tifFiles)->close();
        $pathToFile = public_path('storage/zip/pod.zip');
        $name = $fileName . '.zip';
        $headers = array(
            'Content-Type' => 'application/octet-stream',
        );

        return response()->download($pathToFile, $name, $headers);
    }

    public static function loadtracinglocation($data)
    {
        $b = '1';
        $delete = DB::connection('as400')->table('WEBLIB.WLOCATION')->delete();

        $callLocation = DB::connection('as400')->statement(DB::raw("CALL WEBLIB.LOADTRACE('$data[loadno]', '$b')"));
        $loadlocation = DB::connection('as400')->table('WEBLIB.WLOCATION')->get();

        return $loadlocation;
    }


    public function ppo(Request $request)
    {
        Log::info('LoadController::PPO Loads count and counts per state.');

        $julian_today= date('Yz') + 1;
        //Instate CA only, no outbound loads
        $where_ca = "ORPDAT >= $julian_today AND ORBILL<>'PTSIEX'
                                             AND ORCUST<>'PTSIEX' 
                                             AND ORADIN='PPO' 
                                             AND OROST='CA' AND ORDST='CA'
                                             AND (ORSTAT='D' AND 'ORDSP#'<>'ORLD#' OR ORSTAT='A')";

        // DB::enableQueryLog('as400');

        $california = DB::connection('as400')->table('ITSR5FILE.ORDER as ORDER')
                                             ->select( DB::raw('OROST, count(*) as NOOFLOADS') )
                                             ->join('ITSR5FILE.CITIES as ORG', function ($join) {
                                                    $join->on('OROST' , '=' , 'ORG.CIST');
                                                    $join->on('OROCTY', '=' , 'ORG.CICTY');
                                             })
                                             ->whereRaw($where_ca)
                                             ->groupBy('OROST');

        $where = "ORPDAT >= $julian_today AND ORBILL<>'PTSIEX' 
                                          AND ORCUST<>'PTSIEX' 
                                          AND ORADIN='PPO' AND OROST<>'CA'
                                          AND (ORSTAT='D' AND 'ORDSP#'<>'ORLD#' OR ORSTAT='A')";

        $allOthers = DB::connection('as400')->table('ITSR5FILE.ORDER as ORDER')
                                            ->select( DB::raw('OROST, count(*) as NOOFLOADS') )
                                            ->join('ITSR5FILE.CITIES as ORG', function ($join) {
                                                $join->on('OROST' , '=' , 'ORG.CIST');
                                                $join->on('OROCTY', '=' , 'ORG.CICTY');
                                            })
                                            ->whereRaw($where)
                                            ->groupBy('OROST');
        
        $orders = $california->union($allOthers)->orderBy('OROST')
                                                ->get()
                                                ->toArray();
        
        //Log::info('Orders: ', $orders);
        return $orders;
    }


    public function ppoState(Request $request)
    {
        Log::info('LoadController::PPO Loads per state : '.$request->input('state'));

        $sort = $request->input('sort');
        $sortDirection = $request->input('direction', 'ASC');

        // take MX into account when requested state is TX
        $detailsState = $request->input('state') == 'TX' ? "AND (OROST = 'TX' OR OROST = 'MX')" 
                                                         : ($request->input('state') == 'CA' ? "AND (OROST = 'CA' AND ORDST = 'CA')"
                                                                                             : "AND OROST = '".$request->input('state')."'");

        $julian_today= date('Yz') + 1;
        $where = "ORPDAT >= $julian_today AND ORBILL<>'PTSIEX' 
                                          AND ORCUST<>'PTSIEX' 
                                          AND ORADIN='PPO' 
                                          AND (ORSTAT='D' AND 'ORDSP#'<>'ORLD#' OR ORSTAT='A') ".
                                          $detailsState;
        
        //Log::info('WHERE clause:'.$where."\n");

        $orders = DB::connection('as400')->table('ITSR5FILE.ORDER as ORDER')
                                         ->select('ORPDAT','ORPTIM','ORAPDT','ORAPTM', 'OROCTY', 'ORG.CINAME as ORGCITY', 'OROST', 
                                                  'ORDCTY','DEST.CINAME as DESTCITY', 'ORDST', 'ORDDAT','ORSTP# as ORSTOPS',
                                                  'ORWGT','ORODR# as ORDERNO','ORMILE')
                                         ->join('ITSR5FILE.CITIES as ORG', function ($join) {
                                                $join->on('OROST' , '=' , 'ORG.CIST');
                                                $join->on('OROCTY', '=' , 'ORG.CICTY');
                                         })
                                         ->join('ITSR5FILE.CITIES as DEST', function ($join) {
                                                $join->on('ORDST' , '=' , 'DEST.CIST');
                                                $join->on('ORDCTY', '=' , 'DEST.CICTY');
                                         })
                                         ->when($sort, function ($query) use ($sort, $sortDirection){
                                                return $query->orderBy($sort, $sortDirection);
                                         })
                                         ->whereRaw($where)
                                         ->orderBy('ORPDAT', 'ASC')
                                         ->orderBy('ORPTIM', 'ASC')
                                         ->get()
                                         ->toArray();

        //Log::info($orders);
        return $orders;
    }
    
}
