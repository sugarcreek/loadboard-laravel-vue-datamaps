<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
     protected $connection = 'ourDBServer';
     protected $table = 'ORDER';


     public function extension()
     {
        return $this->hasOne('App\Order', 'ORODR#', 'ORODR#');
     }

     public function trailer()
     {
         return $this->hasOne('App\Truck', 'TRORD#', 'ORODR#');
     }
}
